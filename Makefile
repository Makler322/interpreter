prog: main.cpp LexemParser.o SyntaxParser.o Poliz.o Executer.o
	g++ -g -Wall main.cpp LexemParser.o SyntaxParser.o Poliz.o Executer.o -o prog
	rm -rf *.o
LexemParser.o: LexemParser.h LexemParser.cpp 
	g++ -g -c -Wall LexemParser.cpp -o LexemParser.o
SyntaxParser.o: SyntaxParser.h SyntaxParser.cpp 
	g++ -g -c -Wall SyntaxParser.cpp -o SyntaxParser.o
Poliz.o: Poliz.h Poliz.cpp 
	g++ -g -c -Wall Poliz.cpp -o Poliz.o
Executer.o: Executer.h Executer.cpp 
	g++ -g -c -Wall Executer.cpp -o Executer.o
