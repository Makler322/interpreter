#include "SyntaxParser.h"
#include "LexemParser.h"
#include <stack>


void Parser::gl() {
    curr_lex = scan.get_lex ();
    c_type = curr_lex.get_type ();
    c_val = curr_lex.get_value ();
}

void Parser::analyze () {
    gl();
    Parser::P();
    if (c_type != LEX_FIN){
        throw curr_lex;
    }
    /*
    for ( int i = 0; i < poliz.freedom(); i++) {
        cout << poliz[i] << endl;
    }
    cout << endl << "Yes!!!" << endl;
    */
}

void Parser::P() {
    if (c_type == LEX_PROGRAM){
        gl();
    }
    else{
        throw curr_lex;
    } 
    D1();
    if (c_type == LEX_SEMICOLON){
        gl();
    }
    else{
        throw curr_lex;
    }
    B();
}

void Parser::D1()
{
    if (c_type == LEX_VAR)
    {
        gl();
    }
    else{
        //cout << "gg wp";
         throw curr_lex;
    }
       
    D();
    gl();
    while (c_type == LEX_COMMA)
    {
        gl();
        D();
        if (c_type != LEX_BEGIN)
            gl();
    }
}

void Parser::B()
{
    if (c_type == LEX_BEGIN)
    {
        gl();
    }
    else
        throw curr_lex;

    S();
    while (c_type == LEX_SEMICOLON)
    {
        gl();
        S();
    }
    if (c_type == LEX_END)
        gl();
    else 
        throw curr_lex;
}

void Parser::D()
{
    if (c_type == LEX_ID)// ident
    {
        st_int.push(c_val);
        gl();
        while (c_type == LEX_COMMA) // ,
        {
            gl();
            if (c_type != LEX_ID)
                throw curr_lex;
            st_int.push(c_val);
            gl();
        }
        if (c_type == LEX_COLON) // :
        {
            gl();
            if ((c_type == LEX_INT) || (c_type == LEX_BOOL) ||(c_type == LEX_CHAR) || (c_type == LEX_ARRAY) || (c_type == LEX_LETTER))
            {
                if (c_type != LEX_ARRAY)
                    dec(c_type);

                if (c_type == LEX_ARRAY)
                {
                    gl();
                    if (c_type == LEX_kvadratnye_skobka_levaya) 
                    {
                        gl();
                        if (c_type == LEX_NUM)
                        {
                            gl();
                            if (c_type == LEX_kvadratnye_skobka_pravaya) 
                            {
                                gl();
                                if (c_type == LEX_INT || c_type == LEX_BOOL || c_type == LEX_CHAR)
                                {
                                    dec(c_type);// ???????????????????????????????    
                                    
                                    return;
                                }
                                else
                                    throw curr_lex;
                            }
                            else   
                                throw curr_lex;

                        }
                        else
                            throw curr_lex;
                    }
                    else
                        throw curr_lex;
                }
            }
            else
                throw curr_lex;
        }
        else
            throw curr_lex;
    }
    else 
        throw curr_lex;
}

void Parser::S()
{
    int pl0, pl1, pl2, pl3;
    switch(c_type)
    {
        case LEX_ID:
            check_id();
            poliz.put_lex(Lex(POLIZ_ADDRESS, c_val));
            gl();
            if (c_type == LEX_ASSIGN)
            {
                gl();
                E();
                eq_type();
                poliz.put_lex(Lex(LEX_ASSIGN));
            }
            else
                throw curr_lex;
        break;

        case LEX_IF:
            gl();
            E();
            eq_bool();
            pl2 = poliz.get_free();
            poliz.blank();
            poliz.put_lex(Lex(POLIZ_FGO));
            S();
            pl3 = poliz.get_free();
            poliz.blank();
            poliz.put_lex(Lex(POLIZ_GO));
            poliz.put_lex(Lex(POLIZ_LABEL, poliz.get_free()), pl2);
            if (c_type == LEX_ELSE)
            {
                gl();
                S();
                poliz.put_lex(Lex(POLIZ_LABEL, poliz.get_free()), pl3);
            }
            else throw curr_lex;
        break;

        case LEX_WHILE:
            gl();
            pl0 = poliz.get_free();
            poliz.blank();
            E();
            eq_bool();
            pl1 = poliz.get_free();
            poliz.blank();
            poliz.put_lex(Lex(POLIZ_FGO));
            S();
            poliz.put_lex(Lex(POLIZ_LABEL, pl0), pl0);
            poliz.put_lex(Lex(POLIZ_GO));
            poliz.put_lex(Lex(POLIZ_LABEL, poliz.get_free()), pl1);
        break;

        case LEX_BEGIN:
            B();
        break;

        case LEX_READ:
            gl();
            if (c_type == LEX_LPAREN)
            {
                gl();
                if (c_type == LEX_ID)
                {
                    check_id_in_read();
                    poliz.put_lex(Lex(POLIZ_ADDRESS, c_val));
                    gl();
                    if (c_type !=  LEX_RPAREN)
                        throw curr_lex;
                    gl();
                }
                else throw curr_lex;
            }
            else throw curr_lex;
            poliz.put_lex(Lex(LEX_READ));
        break;

        case LEX_WRITE:{
            int flagggg = 0;
            gl();
            if (c_type == LEX_LPAREN)
            {
                gl();
                if (c_type == LEX_LETTER)
                    flagggg = 1;
                if (c_type == LEX_ID)
                {
                    if (TID[curr_lex.get_value()].get_type() == LEX_LETTER)
                        flagggg = 1;
                }
                E();
                if (c_type != LEX_RPAREN)
                    throw curr_lex;
                gl();
            }
            if (flagggg == 0)
                poliz.put_lex(Lex(LEX_WRITE));
            else
            {
                poliz.put_lex(Lex(LEX_STRWRITE));
            }
            
        break;
        }
        default:
            throw curr_lex;
    }
}

void Parser::E()
{
    E1();
    if ((c_type == LEX_EQ) ||
        (c_type == LEX_LSS) || 
        (c_type == LEX_GTR) ||
        (c_type == LEX_LEQ) || 
        (c_type == LEX_GEQ) || 
        (c_type == LEX_NEQ))
    {
        st_lex.push(c_type);
        gl();
        E1();
        check_op();
    }

}

void Parser::E1()
{
    T();
    while ((c_type == LEX_PLUS) ||
          (c_type == LEX_MINUS) ||
          (c_type == LEX_OR))
    {
        st_lex.push(c_type);
        gl();
        T();
        check_op();
    }
}

void Parser::T()
{
    F();
    gl();
    while((c_type == LEX_TIMES) ||
          (c_type == LEX_AND) ||
          (c_type == LEX_SLASH))
    {
        st_lex.push(c_type);
        gl();
        F();
        check_op();
        gl();
    }
}

void Parser::F()
{
    switch(c_type)
    {
        case LEX_ID:
            check_id();
            poliz.put_lex(curr_lex);
        break;

        case LEX_NUM:
            st_lex.push(LEX_INT);
            poliz.put_lex(curr_lex);
        break;

        case LEX_BOOL:
            st_lex.push(LEX_BOOL);
            poliz.put_lex(curr_lex);
        break;

        case LEX_LETTER:
            st_lex.push(LEX_LETTER);  
            poliz.put_lex(curr_lex);  
        break;

        case LEX_NOT:
            gl();
            F();
            check_not();
        break;

        case LEX_LPAREN:
            gl();
            E();
            if (c_type != LEX_RPAREN)
                throw curr_lex;
        break;

        default:
            throw curr_lex;
    }
}

template <typename T, typename T_EL> void from_st(T& t, T_EL & x)
{
    x = t.top();
    t.pop();
}

void Parser::dec(type_of_lex type)
{
    int i;
    while(!st_int.empty())
    {
        from_st(st_int, i);
        if (TID[i].get_declare())
        {
            throw "ERROR: twice declaration";
        }
        else
        {
            TID[i].put_declare();
            TID[i].put_type(type);
        }
    }
}

void Parser::check_id()
{
    if (TID[c_val].get_declare())
        st_lex.push(TID[c_val].get_type());
    else
        throw "ERROR: not declared";
}

void Parser::check_op ()
{
    type_of_lex t1, t2, op, t = LEX_INT, res = LEX_BOOL;
    from_st(st_lex, t2);
    from_st(st_lex, op);
    from_st(st_lex, t1);

    if (op == LEX_PLUS || op == LEX_MINUS || op == LEX_TIMES || op == LEX_SLASH)
    {
        if (t1 == LEX_INT)
            res = LEX_INT;
        else if (op == LEX_PLUS)
        { 
            res = LEX_LETTER;
            t = LEX_LETTER;
        }

    }

    if (op == LEX_OR || op == LEX_AND)
        t = LEX_BOOL;

    if (op == LEX_ASSIGN)
    {
        if (t1 == LEX_INT)
            res = LEX_INT;

        if (t1 == LEX_BOOL)
            res = LEX_BOOL;

        if (t1 == LEX_CHAR)
            res = LEX_CHAR;

        if (t1 == LEX_LETTER)
            res = LEX_LETTER;
    }
    if (op == LEX_LSS || op == LEX_GTR || op == LEX_LEQ || op == LEX_GEQ )
        if (t1 == LEX_LETTER)
            t = LEX_LETTER;
   // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
    if (t1 == t2 && t1 == t)
        st_lex.push(res);
    else
        throw "Wrong types are in operation";

    poliz.put_lex(Lex(op));
}

void Parser::check_not()
{
    if (st_lex.top() != LEX_BOOL)
        throw "ERROR: wrong type is in not";
    else
        poliz.put_lex(Lex(LEX_NOT));
}


void Parser::eq_type () 
{
    type_of_lex t; 
    from_st(st_lex, t);
    if (t != st_lex.top()){
        throw "wrong types in =";
    }
        
    st_lex.pop();
}

void Parser::eq_bool () 
{
    if ( st_lex.top() != LEX_BOOL )
        throw "ERROR: expression is not boolean";
    st_lex.pop();
}

void Parser::check_id_in_read () 
{
    if ( !TID [c_val].get_declare() )
        throw "ERROR: not declared";
}
