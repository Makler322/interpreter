#include "Poliz.h"
#include <stack>
#include <vector>

using namespace std;

template <typename T, typename T_EL> void from_st(T t, T_EL & x){
    x = t.top();
    t.pop();
}



void Poliz::put_lex(Lex l) { 
    p[free]=l; ++free; 
};
void Poliz::put_lex(Lex l, int place) { 
    p[place]=l;
};
void Poliz::blank() { 
    ++free; 
};
int Poliz::get_free() { 
    return free; 
};
Lex& Poliz::operator[] ( int index ){
    if (index > size())
        throw "POLIZ: Выход за границы массива";
    else
        if ( index > free )
            return p[index];
            //throw "POLIZ:indefinite element of array";
        else
            return p[index];
};

void Poliz::operator=(Poliz& A){
    sizen = A.size();
    delete[] p;
    p = new Lex[sizen];
    free = A.freedom();
    for (int i = 0; i < sizen; i++){
        p[i] = A.p[i];
    }
}

void Poliz::print() {
    for ( int i = 0; i < free; ++i )
        cout << p[i].get_type() <<" "<<p[i].get_string()  << endl;
};

