#pragma once
#include "LexemParser.h"
#include "SyntaxParser.h"
#include <stack>
#include <vector>

using namespace std;

class Executer {
public:
    void execute (Poliz & poliz);
};
class Interpretator {
    Parser pars;
    Executer E;
public:
    Interpretator (const char * program) : pars(program) { };
    void interpretation();
};