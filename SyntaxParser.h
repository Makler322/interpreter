#pragma once

#include "LexemParser.h"
#include "Poliz.h"

#include <stack>
using namespace std;
class Parser {
    Lex curr_lex;
    type_of_lex c_type;
    int c_val;
    Scanner scan;
    stack < int > st_int;
    stack < type_of_lex> st_lex;
    
    void P(); 
    void D1(); 
    void D(); 
    void B(); 
    void S();
    void E(); 
    void E1(); 
    void T(); 
    void F();
    void dec (type_of_lex type); 
    void check_id ();
    void check_op (); 
    void check_not (); 
    void eq_type ();
    void eq_bool (); 
    void check_id_in_read ();
    void gl();
public:
    Poliz poliz;
    Parser (const char *program) : scan (program), poliz(1000) {}
    void analyze ( );
    
};
