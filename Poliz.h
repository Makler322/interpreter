#pragma once
#include "LexemParser.h"
#include <stack>
#include <vector>

using namespace std;

class Poliz {
    Lex *p;
    int sizen;
    int free;
public:
    Poliz(int maxxx){
        sizen = maxxx;
        p = new Lex[sizen];
        free = 0;
    };
    ~Poliz() { 
        delete []p; 
    };
    int size(){
        return sizen;
    }
    int freedom(){
        return free;
    }
    void put_lex(Lex l);
    void put_lex(Lex l, int place);
    void blank();
    int get_free();
    Lex& operator[] (int index);
    void operator=(Poliz& A);
    void print();
};